import imageRenderer from '@sitevision/api/server/ImageRenderer';
import utils from '@sitevision/api/server/Utils';

export function renderImageFromNode(imageNode, imageAlt, imageSizeObj) {
    let articleImage = "";
    if (imageNode !== "" && imageNode !== undefined) {
        let imageScaler = utils.getImageScaler(imageSizeObj.height, imageSizeObj.width);
        imageRenderer.setImageScaler(imageScaler);
        imageRenderer.update(imageNode);
        imageRenderer.setDescription(imageAlt);
        imageRenderer.forceUseImageScaler();
        articleImage = imageRenderer.render();
    }
    return articleImage;
}

export function getImageSize(imageSize) {
    let imageSizeObj = "";
    if (imageSize === "small") {
        imageSizeObj = {
            height: 400,
            width: 400
        };
    }
    else if (imageSize === "medium") {
        imageSizeObj = {
            height: 800,
            width: 800
        };
    }
    else if (imageSize === "large") {
        imageSizeObj = {
            height: 1200,
            width: 1200
        };
    }
    else {
        imageSizeObj = {
            height: 800,
            width: 800
        };
    }
    return imageSizeObj;
}