# Sitevision functions
This is a Sitevision package for handeling Westbridge standard functions

## ImageRenderer
```js
/**
 * @param {items} Array
 * @param {maxItems} number
 * @returns array 
*/
function getImageSize(imageSize)
```

```js
/**
 * @param {imageNode} Node
 * @param {imageAlt} string
 * @param {imageSizeObj} number
 * @returns HTML image element 
*/
function renderImageFromNode(imageNode, imageAlt, imageSizeObj)
```

## LinkRenderer
```js
/**
 * @param {pageNode} Node
 * @param {text} string
 * @returns HTML link element 
*/
function renderLinkFromNode(pageNode, text)
```

## Arrays
```js
/**
 * @param {items} Array
 * @returns array with the newest arrays first
*/
function sortArraysOnDate(items)
```

```js
/**
 * @param {items} Array
 * @param {maxItems} number amout of arrays to be returned af function exectution
 * @returns array 
*/
function getNumberOfArrays(items, maxitems)
```