import linkRenderer from '@sitevision/api/server/LinkRenderer';

export function renderLinkFromNode(pageNode, text) {
    let link = "";
    if (pageNode !== "" && pageNode !== undefined) {
        linkRenderer.update(pageNode);
        linkRenderer.setText(text);
        link = linkRenderer.render();
    }
    return link;
}