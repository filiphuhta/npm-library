export function sortArraysOnDate(items) {
    if (items && items.length > 0) {
        items = items.sort(function (a, b) {
            return new Date(b.date) - new Date(a.date);
        });
    }
    return items;
}

export function getNumberOfArrays(items, maxitems) {
    if (items && items.length > 0) {
        items = items.slice(0, maxitems);
    }
    return items;
}

